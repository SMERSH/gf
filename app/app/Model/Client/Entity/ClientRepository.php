<?php

declare(strict_types=1);

namespace App\Model\Client\Entity;

/**
 * Репозиторий для клиентов
 */
class ClientRepository
{
    /**
     * Получить клиента по телефону
     *
     * @param string $phone
     * @return Client
     */
    public function getClientByPhone(string $phone): ?Client
    {
        return Client::where('phone', $phone)->first();
    }

    /**
     * Добавить клиента
     *
     * @param Client $client
     *
     * @return Client
     */
    public function add(Client $client): Client
    {
        $client->save();

        return $client;
    }
}
