<?php

declare(strict_types=1);

namespace App\Model\Order\UseCase\Create;

use App\Model\Tariff\Entity\TariffRepository;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Форма создания заказа
 *
 * @property string $phone
 * @property string $name
 * @property string $tariff_id
 * @property string $delivery_day
 * @property string $address
 */
class Request extends FormRequest
{
    /** @var TariffRepository */
    protected $tariffRepository;

    /**
     * Request constructor.
     * @param array $query
     * @param array $request
     * @param array $attributes
     * @param array $cookies
     * @param array $files
     * @param array $server
     * @param null $content
     * @param TariffRepository $tariffRepository
     */
    public function __construct(
        array $query = [],
        array $request = [],
        array $attributes = [],
        array $cookies = [],
        array $files = [],
        array $server = [],
        $content = null,
        TariffRepository $tariffRepository
    ) {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);

        $this->tariffRepository = $tariffRepository;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'phone' => [
                'required',
                'string',
                'max:12',
                'regex:/^((8|\+7)[\-\s]?)?(\(?\d{3}\)?[\-\s]?)?[\d\-\s]{7,10}$/'
            ],
            'name'         => 'required|string|max:255',
            'tariff_id'    => 'required|numeric|exists:tariffs,id',
            'delivery_day' => 'required|string|max:10|date_format:d.m.Y',
            'address'      => 'required|string|max:255',
        ];
    }

    /**
     * @param $validator
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            // Проверяем выбранный день доставки
            if ($this->checkDeliveryDay() === false) {
                $validator->errors()->add('delivery_day', 'Выбранный день доставки невозможен с выбранным тарифом');
            }
        });
    }

    /**
     * Получить массив сообщений об ошибках
     *
     * @return array|string[]
     */
    public function messages()
    {
        return [
            'required'                 => 'Поле ":attribute" обязательно для заполнения',
            'tariff_id.exists'         => 'Выбранного тарифа не существует',
            'delivery_day.date_format' => 'Формат даты должен иметь вид "d.m.Y"',
            'phone.regex'              => 'Номер телефона может содержать цифры, "+", "-", "(", ")"'
        ];
    }

    /**
     * Получить название полей формы
     *
     * @return array|string[]
     */
    public function attributes()
    {
        return [
            'name'         => 'Имя',
            'phone'        => 'Телефон',
            'tariff_id'    => 'Тариф',
            'delivery_day' => 'День доставки',
            'address'      => 'Адрес'
        ];
    }

    /**
     * Метод проверяет дату доставки
     *
     * @return bool
     */
    public function checkDeliveryDay(): bool
    {
        // Выбранный день недели
        $chosenDate = Carbon::createFromFormat('d.m.Y', $this->delivery_day);
        $currentDate = Carbon::today();
        $tariff = $this->tariffRepository->getId($this->tariff_id);

        $currentWeekday = $currentDate->dayOfWeek;

        // Если выбранный день недели попадает в дни доставки для тарифа, кидаем ошибку
        if (in_array($chosenDate->dayOfWeek, $tariff->days)) {
            return false;
        }

        $countDays = 0;
        while (in_array($currentWeekday, $tariff->days)) {
            if (++$currentWeekday > 6) {
                $currentWeekday = 0;
            }
            $countDays++;
        }
        // Минимально допустимая дата доставки
        $currentDate->addDays($countDays);

        return $chosenDate >= $currentDate;
    }
}
