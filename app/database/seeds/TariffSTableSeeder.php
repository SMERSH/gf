<?php

use Illuminate\Database\Seeder;
use App\Model\Tariff\Entity\Tariff;
use App\Model\Tariff\Entity\TariffRepository;

/**
 * Добавление тарифов по умолчанию
 */
class TariffSTableSeeder extends Seeder
{
    /**
     * @var TariffRepository
     */
    protected $repository;

    public function __construct(TariffRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tariffs = [
            [
                'title' => 'Тариф будни',
                'price' => 100000,
                'days'  => [0, 6],
            ],
            [
                'title' => 'Тариф выходные',
                'price' => 150000,
                'days'  => [1, 2, 3, 4, 5],
            ],
            [
                'title' => 'Тариф воскресенье',
                'price' => 400000,
                'days'  => [1, 2, 3, 4, 5, 6],
            ]
        ];

        foreach ($tariffs as $data) {
            $tariff = Tariff::new(
                $data['title'],
                $data['price'],
                $data['days']
            );

            $this->repository->add($tariff);
        }
    }
}
