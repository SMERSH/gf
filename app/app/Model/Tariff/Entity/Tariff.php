<?php

declare(strict_types=1);

namespace App\Model\Tariff\Entity;

/**
 * Модель тарифа
 *
 * @property int $id
 * @property string $title
 * @property int $price
 * @property array $days
 */
class Tariff extends \Eloquent
{
    /**
     * @inheritDoc
     *
     * @var string[]
     */
    protected $fillable = [
        'title',
        'price',
        'days'
    ];

    /**
     * @inheritDoc
     *
     * @var string[]
     */
    protected $casts = [
        'days' => 'array'
    ];

    /**
     * @inheritDoc
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];

    /**
     * Создать новый тариф
     *
     * @param string $title
     * @param int $price
     * @param array $days
     *
     * @return static
     */
    public static function new(string $title, int $price, array $days): self
    {
        return new self([
            'title' => $title,
            'price' => $price,
            'days'  => $days
        ]);
    }

    /**
     * Получить сумму для отображения
     *
     * @return string
     */
    public function getBalanceView(): string
    {
        return number_format($this->price / 100, 2, '.', ' ');
    }
}
