<?php

declare(strict_types=1);

namespace App\Model\Tariff\Entity;

/**
 * Репозиторий для тарифов
 */
class TariffRepository
{
    /**
     * Получить тариф по ID
     *
     * @param $id
     *
     * @return Tariff
     */
    public function getId($id): Tariff
    {
        return Tariff::findOrFail($id);
    }

    /**
     * Добавить тариф
     *
     * @param Tariff $tariff
     *
     * @return Tariff
     */
    public function add(Tariff $tariff): Tariff
    {
        $tariff->save();

        return $tariff;
    }

    /**
     * Получить все тарифы
     */
    public function all()
    {
        return Tariff::all();
    }
}
