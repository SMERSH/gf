<?php

declare(strict_types=1);

namespace App\Model\Order\UseCase\Create;

use App\Model\Client\Entity\Client;
use App\Model\Client\Entity\ClientRepository;
use App\Model\Order\Entity\Order;
use App\Model\Order\Entity\OrderRepository;

/**
 * Обработчик команды создания заказа
 */
class Handler
{
    /**
     * @var ClientRepository
     */
    private $clientRepository;
    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * Handler constructor.
     * @param ClientRepository $clientRepository
     * @param OrderRepository $orderRepository
     */
    public function __construct(
        ClientRepository $clientRepository,
        OrderRepository $orderRepository
    ) {
        $this->clientRepository = $clientRepository;
        $this->orderRepository  = $orderRepository;
    }

    public function handle(Command $command): void
    {
        // Если нет клиента, создаем
        if (!($client = $this->clientRepository->getClientByPhone($command->phone))) {
            // Создать клиента
            $client = $this->clientRepository->add(
                Client::new($command->phone, $command->name)
            );
        }

        // Создаем заказ
        $order = Order::new(
            (int)$client->id,
            (int)$command->tariffId,
            $command->deliveryDay,
            $command->address
        );

        $this->orderRepository->add($order);
    }
}
