<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\OrderCreateResource;
use App\Model\Order\UseCase\Create\Command;
use App\Model\Order\UseCase\Create\Handler;
use App\Model\Order\UseCase\Create\Request;
use App\Model\Tariff\Entity\TariffRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;
use \DB;

/**
 * Контроллер управления заказами
 */
class OrderController extends Controller
{
    /**
     * @var TariffRepository
     */
    protected $tariffRepository;

    public function __construct(TariffRepository $tariffRepository)
    {
        $this->tariffRepository = $tariffRepository;
    }

    /**
     * Получить данные для формы создания заказа
     *
     * @param TariffRepository $tariffRepository
     *
     * @return OrderCreateResource
     */
    public function create(TariffRepository $tariffRepository)
    {
        $tariffs = $tariffRepository->all();

        return new OrderCreateResource([
            'tariffs' => $tariffs
        ]);
    }

    /**
     * Сохранение заказа
     *
     * @param Request $request
     * @param Handler $handler
     *
     * @return Response
     * @throws \Exception
     */
    public function store(Request $request, Handler $handler)
    {
        $command = new Command(
            $request->post('phone'),
            $request->post('name'),
            (int)$request->post('tariff_id'),
            $request->post('delivery_day'),
            $request->post('address')
        );

        try {
            DB::beginTransaction();
            $handler->handle($command);
            DB::commit();
        } catch(\Throwable $e) {
            DB::rollBack();
            return response(['errors' => ['message' => [$e->getMessage()]]], 400);
        }

        return response('Заказ успешно создан', Response::HTTP_CREATED);
    }
}
