<?php

declare(strict_types=1);

namespace App\Model\Order\Entity;

/**
 * Репозиторий для заказов
 */
class OrderRepository
{
    /**
     * Получить заказ по ID
     *
     * @param $id
     *
     * @return Order
     */
    public function getId($id): Order
    {
        return Order::findOrFail($id);
    }

    /**
     * Добавить заказ
     *
     * @param Order $order
     *
     * @return Order
     */
    public function add(Order $order): Order
    {
        $order->save();

        return $order;
    }
}
