<?php

declare(strict_types=1);

namespace App\Http\Resources;

use App\Model\Tariff\Entity\Tariff;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

/**
 * Ресурс для формы создания заказа
 *
 * Class OrderCreateResource
 * @package App\Http\Resources
 */
class OrderCreateResource extends JsonResource
{
    /**
     * @inheritDoc
     *
     * @param Request $request
     *
     * @return array|JsonResponse
     */
    public function toResponse($request)
    {
        $data = ['tariffs' => []];

        /** @var Tariff $tariff */
        foreach ($this->resource['tariffs'] as $tariff) {
            $data['tariffs'][$tariff->id] = [
                'id'    => $tariff->id,
                'title' => $tariff->title,
                'price' => $tariff->getBalanceView(),
                'days'  => $tariff->days,
            ];
        }

        return $data;
    }
}
