<?php

declare(strict_types=1);

namespace App\Model\Client\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * Модель клиента
 *
 * @property int $id
 * @property string $phone
 * @property string $name
 */
class Client extends Model
{
    /**
     * @inheritDoc
     *
     * @var string[]
     */
    protected $fillable = [
        'phone',
        'name'
    ];

    /**
     * @inheritDoc
     *
     * @var string[]
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * @inheritDoc
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];

    /**
     * Создать нового клиента
     *
     * @param string $phone
     * @param string $name
     *
     * @return static
     */
    public static function new(string $phone, string $name): self
    {
        return new self([
            'phone' => $phone,
            'name'  => $name,
        ]);
    }
}
