@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Оформление заказа</h1>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <router-view name="OrderCreate"></router-view>
                <router-view></router-view>
            </div>
        </div>
    </div>
@endsection
