### Инструкция по запуску проекта
1. Выполнить команду `make init` - поднимает рабочее окружение, создает БД, наполняет данными и т.д.

### Адрес проекта
`http://localhost:8080/`

### Допустил упущения:
1. Отсутствие комментариев в коде, документации, тестов.
2. DI через интерфейсы
3. Vuejs первый раз щупал, сильно не пинайте.

По коду, упростил как смог. Если есть вопросы по реализации, готов ответить. Надеюсь, я услышу конструктивную критику.
Если есть критические моменты, готов исправить.

### Решения к задачам
В решениях применил коррелированные запросы.

1. Выбрать третий заказ для каждого клиента ( id, client_id, price)

`SELECT o.*
FROM clients c,
LATERAL (
    SELECT *
    FROM orders
    WHERE c.id = orders.client_id
    ORDER BY orders.id
    LIMIT 1
    OFFSET 2
) o;`

2. Выбрать для каждого клиента третий заказ сделанный после заказа стоимостью больше 1000 ( id, client_id, price)

`SELECT o.*
FROM clients c,
LATERAL (
     SELECT oo.*
     FROM orders o,
     LATERAL (
        SELECT *
        FROM orders oo
        WHERE oo.id > o.id and oo.client_id = c.id
        ORDER BY oo.id
        LIMIT 1
        OFFSET 2
     ) oo
     WHERE c.id = o.client_id and o.price > 1000
     ORDER BY o.id
     LIMIT 1
 ) o;`
