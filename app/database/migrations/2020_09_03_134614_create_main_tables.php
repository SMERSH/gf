<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMainTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Создаем таблицу тарифов
         */
        Schema::create('tariffs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->bigInteger('price')->default(0);
            $table->json('days');
            $table->timestamps();
            $table->softDeletes();
        });

        /**
         * Создаем таблицу клиентов
         */
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('phone')->unique();
            $table->timestamps();
            $table->softDeletes();
        });

        /**
         * Создаем таблицу заказов
         */
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('client_id')
                ->references('id')
                ->on('clients');
            $table->integer('tariff_id')
                ->references('id')
                ->on('tariffs');
            $table->date('delivery_day');
            $table->string('address');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tariffs');
        Schema::dropIfExists('clients');
        Schema::dropIfExists('orders');
    }
}
