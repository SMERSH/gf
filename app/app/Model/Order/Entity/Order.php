<?php

declare(strict_types=1);

namespace App\Model\Order\Entity;

/**
 * Модель заказа
 */
class Order extends \Eloquent
{
    /**
     * @inheritDoc
     *
     * @var string[]
     */
    protected $fillable = [
        'client_id',
        'tariff_id',
        'delivery_day',
        'address'
    ];

    /**
     * @inheritDoc
     *
     * @var string[]
     */
    protected $casts = [
        'delivery_day' => 'date:d.m.Y'
    ];

    /**
     * @inheritDoc
     *
     * @var string[]
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * @inheritDoc
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];

    /**
     * Создать новый заказ
     *
     * @param int $clientId
     * @param int $tariffId
     * @param string $deliveryDay
     * @param string $address
     *
     * @return static
     */
    public static function new(int $clientId, int $tariffId, string $deliveryDay, string $address): self
    {
        return new self([
            'client_id'    => $clientId,
            'tariff_id'    => $tariffId,
            'delivery_day' => $deliveryDay,
            'address'      => $address
        ]);
    }
}
