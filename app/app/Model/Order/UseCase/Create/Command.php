<?php

declare(strict_types=1);

namespace App\Model\Order\UseCase\Create;

/**
 * Класс DTO для создание заказа
 */
class Command
{
    /**
     * @var string
     */
    public $phone;
    /**
     * @var string
     */
    public $name;
    /**
     * @var int
     */
    public $tariffId;
    /**
     * @var string
     */
    public $deliveryDay;
    /**
     * @var string
     */
    public $address;

    public function __construct(
        string $phone,
        string $name,
        int $tariffId,
        string $deliveryDay,
        string $address
    ) {
        $this->phone       = $phone;
        $this->name        = $name;
        $this->tariffId    = $tariffId;
        $this->deliveryDay = $deliveryDay;
        $this->address     = $address;
    }
}
